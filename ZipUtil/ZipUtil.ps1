<#
.SYNOPSIS
    空のzipファイルを作成します。
.DESCRIPTION
    ZipFileNameで指定されたファイル名で空のzipファイルを作成します。
.PARAMETER ZipFileName
    zipファイル名
.INPUTS
    なし
.OUTPUTS
    なし
.NOTES
    なし
.EXAMPLE
    New-Zip foo.zip
#>
function New-Zip([string]$ZipFileName)
{
	Set-Content $ZipFileName ("PK" + [char]5 + [char]6 + ("$([char]0)" * 18))
	(dir $ZipFileName).IsReadOnly = $false
}

<#
.SYNOPSIS
    zipファイルにファイルを追加します。
.DESCRIPTION
    ZipFileNameで指定されたzipファイルにパイプラインで入力されたファイルを追加します。
.PARAMETER ZipFileName
    zipファイル名
.INPUTS
    追加対象ファイル
.OUTPUTS
    なし
.NOTES
    フォルダは追加できません
    すでにzipファイルに追加されているファイルと同名のものは追加できません
.EXAMPLE
     Get-ChildItem C:\bar | Add-Zip foo.zip
#>
function Add-Zip([string]$ZipFileName)
{
	$files = @($input)

	# フォルダがあると待ち合わせできない
	$leafs = $files | ? { Test-Path $_.FullName -PathType Leaf }
	if ($leafs.Count -ne $files.count) {
		echo $files.count
		echo $leafs.count
		throw 'フォルダが含まれています'
	}

	# ファイル名が重複すると待ち合わせできない
	$uniqs = $files | Select Name -Unique
	if ($uniqs.Count -ne $files.Count) {
		throw 'ファイル名が重複しています'
	}

	# zipファイルがまだなければ作成
	if(-not (Test-Path $ZipFileName))
	{
		set-content $ZipFileName ("PK" + [char]5 + [char]6 + ("$([char]0)" * 18))
		(dir $ZipFileName).IsReadOnly = $false
	}

	$shellApplication = New-Object -ComObject Shell.Application
	$zipPackage = $shellApplication.NameSpace((Convert-Path $ZipFileName))

	# zipにすでに同名のファイルが含まれていると待ち合わせできない
	$allFiles = $zipPackage.Items() + $files
	$uniqs = $allFiles | Select Name -Unique
	if ($uniqs.Count -ne $allFiles.Count) {
		throw 'ファイル名が重複しています'
	}

	$files | % {
		$count = $zipPackage.Items().Count
		$zipPackage.CopyHere($_.FullName)
		# ファイルカウントにより終了を待ち合わせ
		while ($zipPackage.Items().Count -eq $count)
		{
			Start-sleep -milliseconds 100
		}
	}
}

<#
.SYNOPSIS
    ファイルリストからzipファイルを作成します。
.DESCRIPTION
    Filesで指定されたファイルリストからZipFileNameで指定されたzipファイルを作成します。
.PARAMETER ZipFileName
    zipファイル名
.PARAMETER Files
    対象ファイル
.INPUTS
    なし
.OUTPUTS
    なし
.NOTES
    フォルダは追加できません
    同名のファイルは追加できません
.EXAMPLE
     To-Zip foo.zip (Get-ChildItem C:\bar)
#>
function To-Zip([string]$ZipFileName, [array]$Files)
{
	# zipがすでに出来ている
	if(Test-Path $ZipFileName)
	{
		throw "[$ZipFileName]がすでに存在します。"
	}

	# フォルダが含まれていると待ち合わせできない
	$leafs = $Files | ? { Test-Path $_.FullName -PathType Leaf }
	if ($leafs.Count -ne $Files.count) {
		throw 'フォルダが含まれています'
	}

	# ファイル名が重複すると待ち合わせできない
	$uniqs = $Files | Select Name -Unique
	if ($uniqs.Count -ne $Files.Count) {
		throw 'ファイル名が重複しています'
	}

	set-content $ZipFileName ("PK" + [char]5 + [char]6 + ("$([char]0)" * 18))
	(dir $ZipFileName).IsReadOnly = $false

	$shellApplication = New-Object -ComObject Shell.Application

	$zipPackage = $shellApplication.NameSpace((Convert-Path $ZipFileName))

	$Files | % {
		$count = $zipPackage.Items().Count
		$zipPackage.CopyHere($_.FullName)
		# ファイルカウントにより終了を待ち合わせ
		while ($zipPackage.Items().Count -eq $count)
		{
			Start-sleep -milliseconds 100
		}
	}
}

<#
.SYNOPSIS
    zipファイルの内容を表示します。
.DESCRIPTION
    ZipFileNameで指定されたzipファイルの内容を表示します。
.PARAMETER ZipFileName
    zipファイル名
.INPUTS
    なし
.OUTPUTS
    なし
.NOTES
    なし
.EXAMPLE
     Get-Zip foo.zip
#>
function Get-Zip([string]$ZipFileName)
{
	if(-not (Test-Path $ZipFileName))
	{
		return
	}

	$shellApplication = New-Object -ComObject Shell.Application
	$zipPackage = $shellApplication.NameSpace((Convert-Path $ZipFileName))
	$zipPackage.Items() | Select-Object Path
}

<#
.SYNOPSIS
    zipファイルを解凍します。
.DESCRIPTION
    ZipFileNameで指定されたzipファイルをDestinationで指定されたフォルダ配下に解凍します。
.PARAMETER ZipFileName
    zipファイル名
.PARAMETER Destination
    解凍先フォルダ名
.INPUTS
    なし
.OUTPUTS
    なし
.NOTES
    なし
.EXAMPLE
     Extract-Zip foo.zip baz
#>
function Extract-Zip([string]$ZipFileName, [string]$Destination)
{
	if(-not (Test-Path $ZipFileName))
	{
		return
	}

	if (-not (Test-Path $Destination -PathType Container))
	{
		New-Item $Destination -ItemType Directory
	}

	$shellApplication = New-Object -ComObject Shell.Application
	$zipPackage = $shellApplication.NameSpace((Convert-Path $ZipFileName))
	$destinationFolder = $shellApplication.NameSpace((Convert-Path $Destination))
	$destinationFolder.CopyHere($zipPackage.Items())
}
