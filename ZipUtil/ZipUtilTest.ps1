${cwd} = Split-Path $MyInvocation.MyCommand.Path -Parent
. "${cwd}\zip_util.ps1"

${yesterday} = (Get-Date).date.AddDays(-1)
${logDir} = "C:\logs"
${zipDate} = ${yesterday}.ToString("yyyyMMdd")
${zipFile} = "${cwd}\logs_${zipDate}.zip"

Get-ChildItem ${logDir} -Recurse | ? {
  $_.LastWriteTime -lt ${yesterday}
} | Add-Zip ${zipFile}
